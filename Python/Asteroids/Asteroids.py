from random import random

from SpaceObject import SpaceObject
import pyglet
import main
import math
import numpy as np
import random


class Asteroids(SpaceObject):
    def __init__(self):

        asteroidImg = pyglet.image.load('images/meteorGrey_big3.png')
        asteroidImg.anchor_x = asteroidImg.width // 2
        asteroidImg.anchor_y = asteroidImg.height // 2
        asteroid = pyglet.sprite.Sprite(asteroidImg, batch=main.batch)
        asteroid.x = random.randint(0, 360)
        asteroid.y = 0
        asteroid.speed = 15
        asteroid.rotation = random.randint(0, 360)
        asteroid.death = False
        asteroid.vel = np.array(
            [math.sin(math.radians(asteroid.rotation)),
             math.cos(math.radians(asteroid.rotation))]) * asteroid.speed
        return asteroid

    def move(self):
        print()
        Asteroids.vel += np.array(
            [math.sin(math.radians(Asteroids.rotation)),
             math.cos(math.radians(Asteroids.rotation))]) * Asteroids.speed

    def delete(self):
        self.asteroid.death = True
        self.delete()
        #self.asteroid.main.batch.delete()
