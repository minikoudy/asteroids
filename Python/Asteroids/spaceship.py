import pyglet
import numpy as np
from main import window
import math
from main import batch

from SpaceObject import SpaceObject


class Spaceship(SpaceObject):
    def __init__(self):
        lodImg = pyglet.image.load('images/spaceship.png')
        lodImg.anchor_x = lodImg.height // 2
        lodImg.anchor_y = lodImg.width // 2
        lod = pyglet.sprite.Sprite(lodImg, batch=batch)
        lod.vel = np.array([5, 5], dtype=np.float)
        lod.x = 50
        lod.y = 50
        lod.shoot = 0
        lod.speed = 2
        lod.rotation = 0
        return lod


