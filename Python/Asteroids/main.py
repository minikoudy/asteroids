# This is a sample Python script.
from random import random
import pyglet
import math
import numpy as np
from pyglet import window, gl
from pyglet.window import mouse
import spaceship as Spaceship
import laser as Laser
import Asteroids as Asteroids

global batch
batch = pyglet.graphics.Batch()
objects = []


def klik(x, y, button, modifiers):
    global laser
    if button == mouse.LEFT:
        if Spaceship.shoot == 0:
            laser = Laser.Laser.__init__(Laser, Spaceship.x, Spaceship.y, Spaceship.rotation)
            laser.puvodniX = Spaceship.x
            laser.puvodniY = Spaceship.y
            objects.append(laser)
            laser.batch.draw()
            Spaceship.shoot = 1

        # laser.rotation = Spaceship.rotation
        # batch.__reduce__(laser)


def move(text):
    Spaceship.vel += np.array(
        [math.sin(math.radians(Spaceship.rotation)),
         math.cos(math.radians(Spaceship.rotation))]) * Spaceship.speed
    if text == 'a':
        Spaceship.rotation = (Spaceship.rotation - 5) % 360
    elif text == 'd':
        Spaceship.rotation = (Spaceship.rotation + 5) % 360
    # super().Spaceship.move(text)


def tik(t, shoot=None):
    Spaceship.x = (Spaceship.x + Spaceship.vel[0] * t) % window.width
    Spaceship.y = (Spaceship.y + Spaceship.vel[1] * t) % window.height
    if asteroid.death == False:
        asteroid.x = (asteroid.x + asteroid.vel[0] * t) % window.width
        asteroid.y = (asteroid.y + asteroid.vel[1] * t) % window.height
    if Spaceship.shoot == 1:
        laser.x = (laser.x + laser.vel[0] * t) % window.width
        laser.y = (laser.y + laser.vel[1] * t) % window.height
        laser.vel += np.array(
            [math.sin(math.radians(laser.rotation)), math.cos(math.radians(laser.rotation))]) * 40
        distance = math.sqrt((laser.puvodniX - laser.x) ** 2 + (laser.puvodniY - laser.y) ** 2)
        if distance > 400:
            laser.delete()
            Spaceship.shoot = 0
    kolize()


def kolize():
    distanceSpaceshipAsteroid = math.sqrt(
        ((Spaceship.position[0] - asteroid.position[0]) ** 2 + (Spaceship.position[1] - asteroid.position[1])) ** 2)
    if distanceSpaceshipAsteroid <= math.sqrt(Spaceship.width**2+Spaceship.height**2):
        Spaceship.delete()
        pyglet.app.exit()
        return False
    if Spaceship.shoot == 1:
        distanceStrelaAsteroid = math.sqrt(
            ((laser.position[0] - asteroid.position[0]) ** 2 + (laser.position[1] - asteroid.position[1])) ** 2)
        if distanceStrelaAsteroid <= math.sqrt(asteroid.width**2+asteroid.height**2):
            asteroid.delete()
            return True



def vykresli():
    window.clear()
    batch.draw()
    Spaceship.batch.draw()
    asteroid.batch.draw()
    for x_offset in (-window.width, 0, window.width):
        for y_offset in (-window.height, 0, window.height):
            gl.glPushMatrix()
            gl.glTranslatef(x_offset, y_offset, 0)
            batch.draw()
            asteroid.batch.draw()
            gl.glPopMatrix()


if __name__ == '__main__':
    width = 1024
    height = 860
    title = "Asteroids"
    window = pyglet.window.Window(width, height, title)
    # window.set_fullscreen()

    Spaceship = Spaceship.Spaceship.__init__(Spaceship)
    Spaceship.x = window.width // 2
    Spaceship.y = window.height // 2
    objects.append(Spaceship)
    asteroid = Asteroids.Asteroids.__init__(Asteroids)
    objects.append(asteroid)
    pyglet.clock.schedule_interval(tik, 1 / 30)  # 1 = 1sec
    window.push_handlers(
        on_text=move,
        on_draw=vykresli,
        on_mouse_press=klik,

    )
    pyglet.app.run()
    print('Hotovo!')
