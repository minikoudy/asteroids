import pyglet
import numpy as np
from pyglet import window
import math
import main


class Laser:
    def __init__(self, x, y, rotation):
        laserImg = pyglet.image.load('images/laser.png')
        laserImg.anchor_x = laserImg.width // 2
        laserImg.anchor_y = laserImg.height // 2
        laser = pyglet.sprite.Sprite(laserImg, batch=main.batch)
        laser.rotation = rotation
        laser.vel = np.array([5, 5], dtype=np.float)
        laser.x = (x + laser.vel[0])
        laser.y = (y + laser.vel[1])
        laser.vel += np.array(
            [math.sin(math.radians(rotation)), math.cos(math.radians(rotation))]) * 2
        # if laser.x == window.width or laser.y == window.height:
        #     laser.delete()
        return laser

    def delete(self):
        self.laser.delete()
        # self.laser.main.batch.delete()


